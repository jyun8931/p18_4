import java.util.ArrayList;
import java.util.ListIterator;

public class P18_4 {

    public static  <T extends Measurable<T>>  T largest(ArrayList<T> list){
        //Initialize the answer
        T ans = list.get(0);
        //Loop through all elements
        for(T obj : list){
            //Check the max, and update the max
            if(obj.getMeasure()>ans.getMeasure()){
                ans = obj;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        Measurable[] elements = new Measurable[5];
        elements[0] = new Measurable<Integer>(3);
        elements[1] = new Measurable<Double>(5.5);
        elements[2] = new Measurable<Double>(99.0);
        elements[3] = new Measurable<Integer>(1);


        ArrayList<Measurable> list = new ArrayList();
        list.add(elements[0]);
        list.add(elements[1]);
        list.add(elements[2]);
        list.add(elements[3]);
        System.out.println("Largest is "+largest(list).getMeasure());
    }
}