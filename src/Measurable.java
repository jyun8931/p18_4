import java.util.ArrayList;

public class Measurable<T> {
    private T obj;

    public Measurable(T obj)
    {
        this.obj = obj;
    }

    public double getMeasure() {
        if(obj instanceof Integer)
            return ((Integer)((Object)obj));
        else
            return (((double)((Object)obj)));
    }
}
